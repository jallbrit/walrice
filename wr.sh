#!/usr/bin/env bash

action="rice"
wallpaperDir=~/Pictures/wallpapers
walCmd="wal -qg"

setWall=0
wallMode="fill"
setTermColors=1
helpMe=0
gtk=0
feh_geometry=0
verbose=0
waitTime=3

# get CLI options

OPTS="hd:gwW:f:rs:cClpLvtTDIVm"
LONGOPTS="help,directory:,gtk,skip,wall,wallmode:,geometry:,recursive,waittime:,colors,clean,list,preview,list-images,view,tag,remove-tag,delete-tag,delete-image,verbose,rename"

# getopt using those options; parse through "$@"
parsed=$(getopt --options=$OPTS --longoptions=$LONGOPTS -- "$@")
eval set -- "${parsed[@]}"

while true; do
	case "$1" in
		-h|--help) helpMe=1; shift ;;
		-d|--directory) wallpaperDir="$2"; shift 2; ;;
		-g|--gtk) gtk=1; shift ;;
		   --skip) setTermColors=0; shift ;;
		-w|--wall) setWall=1; shift ;;
		-W|--wallmode) wallMode="$2"; shift 2; ;;
		-f|--geometry) feh_geometry="$2"; shift 2; ;;
		-r|--recursive) action="cycle"; shift ;;
		-m|--rename) action="rename"; shift ;;
		-s|--waittime) waitTime="$2"; shift 2 ;;
		-c|--colors) action="printColors"; shift ;;
		-C|--clean) action="cleanTags"; shift ;;
		-l|--list) action="list"; shift ;;
		-p|--preview) action="preview"; shift ;;
		-L|--list-images) action="listImages"; shift ;;
		-v|--view) action="tagView"; shift ;;
		-t|--tag) action="tagAdd"; shift ;;
		-T|--remove--tag) action="tagRemove"; shift ;;
		-D|--delete-tag) action="tagDelete"; shift ;;
		-I|--delete-image) action="imageDelete"; shift ;;
		-V|--verbose) verbose=1; shift ;;

		--) shift; break ;;
		*)
			printf '%s\n' "error while parsing options"
			exit 1
			;;
	esac
done

HELP="\
Usage: wr [OPTION]... [TAG|TAG/IMAGE|IMAGE]...
Create, manage, and deploy themes. By default, generate a theme based on a given
tag and deploy it by changing the wallpaper and terminal colors.

optional args:
  -h, --help            display this help
  -v, --verbose         make output more verbose
  -d, --directory DIR   set wallpaper directory, which must contain directory
                        named 'images'. Default is '~/Pictures/wallpapers'
  -g, --gtk             generate gtk and qt themes and live reload applications
      --skip            skip changing colors in terminals
  -w, --wall            just set wallpaper and exit
  -W, --wallmode MODE   set how wallpaper will fit screen (see below)
  -f, --geometry=WxH+x+y
                        set feh --geometry; see 'man feh' for more details
  -c, --colors          print current theme's colors
  -p, --preview         preview images inside terminal (requires catimg)
  -l, --list [TAG]      list all tags or images inside tag
  -L, --list-images     list all images
  -r, --recursive       recursively rice to all images inside a tag
  -s, --waittime TIME   time (in seconds) to wait between images during a
                        recursive rice
  -t, --tag IMAGE [TAG]...
                        add tag to image
  -T, --remove-tag IMAGE [TAG]...
                        remove tag from image
  -v, --view            view image's tags
  -D, --delete-tag      delete tag
  -I, --delete-image    delete image
  -m, --rename IMAGE NEW-NAME
                        rename image to new name, and fix tags to point to new image
  -C, --clean           clean up tag database, removing empty tags and dead links

wallpaper modes:
  center: center image on background, adds border if too small
  fill:   preserves aspect ratio, zoom until it fits all edges
  max:    preserves aspect ratio, zoom until it fits one edge
  scale:  fit image to exact edges, stretching it when necessary
  tile:   tile image if smaller than screen"

if [ ! -d "$wallpaperDir" ] || [ ! -d "$wallpaperDir"/images ]; then
	printf '%s\n' "error: cannot access '$wallpaperDir': No such file or directory, or does not contain directory named 'images'"
	exit 1
fi

((helpMe)) && printf '%s\n' "$HELP" && exit 0

printColors() {
	. ~/.cache/wal/colors.sh

	# i owe this one to pywal's creator
	printf '%s' "  color0   color1   color2   color3   color4   color5   color6   color7"
	for i in {0..15}; do
		color="color${i}"
		((i%8==0)) && printf '\n'
		((i!=0)) && t=0
		printf '%b' "\\e[3${t:-7}m\\e[48;5;${i}m ${!color} \\e[0m"
	done
	printf '\n%s\n' "  color8   color9   color10  color11  color12  color13  color14  color15"
}

list() {
	# if no input is given, list all tags
	[ -z "$1" ] && for file in "$wallpaperDir/tags/"*; do
		echo "${file#$wallpaperDir/tags/}"	# remove path prefix
	done | column && return 0

	# otherwise, list contents of given tags
	findTags "$1"

	for tag in "${resultsTags[@]}"; do
		echo "Listing tag '$(basename "$tag")':"
		findTagSlashImages "${tag#$wallpaperDir/tags/}"
		for image in "${resultsTagSlashImages[@]}"; do
			base="$(basename "$image")"
			echo "${base%.*}"
		done | column
		[ "$tag" != "${resultsTags[-1]}" ] && echo
	done
}

listImages() { ls "$wallpaperDir/images" | sed 's/\..*$//' | column; }

reloadGUI() {
	# gtk live reload
	# timeout forces xsettingsd to quit
	if [ "$(command -v xsettingsd)" ]; then
		timeout .01 xsettingsd -c <(echo "Net/ThemeName \"wal\"") &> /dev/null
	else
		echo "skipping gtk theme live reload: xsettingsd not installed"
	fi

	# qt5 live reload
	# replacing config file with itself forces qt5ct to reload
	if [ "$(command -v qt5ct)" ] && [ -f ~/.config/qt5ct/qt5ct.conf ]; then
		mkdir -p /tmp/wr
		temp="$(mktemp /tmp/wr/qt5ct.conf.XXXXXXXX)"
		orig="$HOME/.config/qt5ct/qt5ct.conf"

		cp "$orig" "$temp"
		rm "$orig"
		cp "$temp" "$orig"
	else
		echo "skipping qt theme live reload: qt5ct is not installed or config file (~/.config/qt5ct/qt5ct.conf) not found"
	fi

	# bspwm
	if pgrep bspwm &> /dev/null; then
		bspc config normal_border_color "$color8"
		bspc config active_border_color "$color2"
		bspc config focused_border_color "$color15"
		bspc config presel_feedback_color "$color1"
	fi
}

# use global arrays to store results of findings
declare -g resultsAnything resultsTags resultsImages resultsTagSlashImages

# mapfile ensures that the output of 'find' is interpreted and placed into an array properly.
# 'find' will use the null character for a delimiter with -print0
# 'mapfile' looks for null character delimiter with -d ''
# More at https://stackoverflow.com/questions/23356779/how-can-i-store-the-find-command-results-as-an-array-in-bash

findTags() {
	input="$1"
	mapfile -d '' resultsTags < <(find "$wallpaperDir/tags/" -type d -print0 | fzf -f "$wallpaperDir/tags/$input" --read0 --print0)
}

findImages() {
	input="$1"
	mapfile -d '' resultsImages < <(find "$wallpaperDir/images/" -type f -print0 | fzf -f "$wallpaperDir/images/$input" --read0 --print0 | sort -z)
}

findTagSlashImages() {
	input="$1"
	mapfile -d '' resultsTagSlashImages < <(find "$wallpaperDir/tags/" -type l -print0 | fzf -f "$wallpaperDir/tags/$input" --read0 --print0)
}

findAnything() {
	input="$1"

	findTags "$input"
	resultsAnything=("${resultsTags[@]}")

	if [ ${#resultsAnything[@]} -eq 0 ]; then
		findImages "$input"
		resultsAnything=("${resultsImages[@]}")
	fi
	if [ ${#resultsAnything[@]} -eq 0 ]; then
		findTagSlashImages "$input"
		resultsAnything=("${resultsTagSlashImages[@]}")
	fi
}

theme() {
	image="$1"

	printf '%s\n' "ricing to: ${image#$wallpaperDir/*/}"

	# set wallpaper if that's all we need to do
	if ((setWall)); then
		((verbose)) && echo "using wallpaper fit: $wallMode"

		# contruct feh command
		fehArgs=()
		case "$wallMode" in
			center) fehArgs+=('--bg-center') ;;
			fill) fehArgs+=('--bg-fill') ;;
			max) fehArgs+=('--bg-max') ;;
			scale) fehArgs+=('--bg-scale') ;;
			tile) fehArgs+=('--bg-tile') ;;
		esac
		((feh_geometry)) && fehArgs+=('--geometry' "$feh_geometry")
		feh "${fehArgs[@]}" "$image"
		return 0
	fi

	# construct pywal command
	walArgs=()
	((!setTermColors)) && walArgs+=('-s')	# skip changing colors in terminals
	((!gtk)) && walArgs+=('-e')		# skip reloading gtk/xrdb/i3/sway/polybar

	wal -q "${walArgs[@]}" -i "$image"
	. ~/.cache/wal/colors.sh
	((gtk)) && reloadGUI
}

rice() {
	input="$1"

	if [ -z "$input" ]; then
		files=("$wallpaperDir/images/"*)
		resultsAnything="${files[$RANDOM%${#files[@]}]}"
		theme "${resultsAnything[0]}"
		return 0
	fi

	findAnything "$input"

	# if we found 1 result, continue
	if [ "${#resultsAnything[@]}" -eq 1 ]; then

		# if user specified a directory, choose a random file inside
		if [ -d "${resultsAnything[0]}" ]; then
			((verbose)) && printf '%b\n' "tag specified, choosing random image inside..."
			files=("$resultsAnything/"*)
			resultsAnything="${files[$RANDOM%${#files[@]}]}"
		fi
		theme "${resultsAnything[0]}"

	# if there was more than 1 result, quit
	elif [ "${#resultsAnything[@]}" -gt 1 ]; then
		printf '%s\n' "error: two or more objects match input: "
		for file in "${resultsAnything[@]}"; do
			echo "${file#$wallpaperDir/tags/}"
		done | column
	else
		printf '%s\n' "error: tag or image not found for input '$input'"
	fi
}

cycle() {
	input="$1"

	# if no input is given, cycle through all images
	if [ -z "$input" ]; then
		findImages
		for image in "${resultsImages[@]}"; do
			theme "$image"
			sleep "$waitTime"
		done
		return 0
	fi

	findTags "$input"
	[ ${#resultsTags[@]} -eq 0 ] && echo "error: No matching tags found for input '$1'" && return 1

	# cycle through each tag found
	for tag in "${resultsTags[@]}"; do
		echo "Cycling through '${tag#$wallpaperDir/tags/}':"
		for image in "$tag/"*; do
			[ -f "$image" ] && theme "$image" && sleep $waitTime
		done
		[ "$tag" != "${resultsTags[-1]}" ] && echo
	done
}

preview() {
	input="$1"
	[ -z "$input" ] && echo "error: Must specify something to preview" && return 1

	type catimg &>/dev/null || { echo "error: catimg is not installed."; return 1; }

	findTags "$input"
	tags=("${resultsTags[@]}")
	[ ${#tags} -eq 0 ] && echo "error: No matching tags found for input '$1'" && return 1

	for tag in "${tags[@]}"; do
		for image in "$tag/"*; do
			name="$(basename "$image")"
			name="${name%.*}"
			[ ${#name} -gt 20 ] && name="${name:0:17}..."
			catimg -w 30 "$image"
			printf '%b\n\n' "${name}"
		done | column -e
	done
}

cleanTags() {
	find "$wallpaperDir/tags" -type l \! -exec test -e '{}' \; -delete
	find "$wallpaperDir/tags" -type d -empty -delete
}

imageDelete() {
	input="$1"
	[ -z "$input" ] && echo "error: Must specify image to delete" && return 1

	findImages "$input"
	images=("${resultsImages[@]}")
	[ ${#images} -eq 0 ] && echo "error: No matching images found for input '$1'" && return 1

	for image in "${images[@]}"; do
		read -rp "Are you sure you want to delete image '${images#$wallpaperDir/images/}'? [y/n]: "
		if [ "$REPLY" != "${REPLY#[yY]}" ]; then
			printf '%b\n' "Deleting image '$image'"
			rm "$image"
			cleanTags
		else
			printf '%b\n' "Not deleting image '$image'"
		fi
	done
}

imageRename() {
	local sourceImage="$1"
	local destinationImage="$2"

	[ -z "$destinationImage" ] && echo "error: Must specify source image and destination name" && return 1
	findImages "$sourceImage"
	if [ ${#resultsImages[@]} -gt 1 ]; then
		echo "error: two or more images match input:"
		for file in "${resultsImages[@]}"; do
			echo "${file#$wallpaperDir/tags/}"
		done | column
	elif [ ${#resultsImages[@]} -lt 1 ]; then
		echo "error: No matching images found for input '$1'"
		return 2
	fi

	sourceImage="${resultsImages[0]}"
	((verbose)) && echo "Found source image: $sourceImage"

	# if user did not specify an extension, use source's
	[ -n "${destinationImage##*.*}" ] && destinationImage="${destinationImage}.${sourceImage##*.}"

	# ensure destination has same extension as source
	if [ "${sourceImage##*.}" != "${destinationImage##*.}" ]; then
		echo "error: Destination image name must have same extension as source ('${sourceImage##*.}')"
		return 3
	fi
	destinationImage="$wallpaperDir/images/$destinationImage"
	destinationImageName="$(basename "$destinationImage")"

	read -rp "Are you sure you want to rename ${sourceImage##$wallpaperDir/images/} to $destinationImageName? [y/n]: "
	if [ "$REPLY" != "${REPLY#[yY]}" ]; then

		# move source image
		echo "Renaming '${sourceImage##$wallpaperDir/}' -> '${destinationImage##$wallpaperDir/}'"
		mv "$sourceImage" "$destinationImage"

		# fix tags to use new image name
		findTagSlashImages "${sourceImage##$wallpaperDir/images/}"
		for result in "${resultsTagSlashImages[@]}"; do
			tag="${result%/*}"
			echo "Renaming '${result##$wallpaperDir/}' -> '${tag##$wallpaperDir/}/$destinationImageName'"

			# re-create symbolic link
			rm "$result"
			ln -s "$destinationImage" "$tag/$destinationImageName"
		done
	else
		echo "Not renaming image."
	fi
}

tagDelete() {
	input="$1"
	[ -z "$input" ] && echo "error: Must specify tag to delete" && return 1

	findTags "$input"
	tags=("${resultsTags[@]}")
	[ ${#tags} -eq 0 ] && echo "error: No matching tags found for input '$1'" && return 1

	for tag in "${tags[@]}"; do
		read -rp "Are you sure you want to delete the tag '${tag#$wallpaperDir/tags/}'? [y/n]: "
		if [ "$REPLY" != "${REPLY#[yY]}" ]; then
			printf '%b\n' "Deleting tag '${tag#$wallpaperDir/tags/}'"
			rm -r "$tag"
		else
			echo "Not deleting tag."
			return 0
		fi
	done
}

tagView() {
	input="$1"
	[ -z "$input" ] && echo "error: Must specify image to inspect" && return 1

	findImages "$1"
	images=("${resultsImages[@]}")
	[ ${#images[@]} -eq 0 ] && echo "error: No matching images found for input '$1'" && return 1

	for image in "${images[@]}"; do
		echo "Tags found for image '${image#$wallpaperDir/images/}':"
		findTagSlashImages "${image#$wallpaperDir/images/}"
		for match in "${resultsTagSlashImages[@]}"; do
			echo "$(basename "$(dirname "$match")")"
		done | column
		[ "$image" != "${images[-1]}" ] && echo
	done
}

editImageTags() {
	local action="$1"; shift
	local imageQuery="$1"; shift
	local tags=($@)

	[ -z "$imageQuery" ] || [ -z "$tags" ] && echo "error: Must specify image and tags" && exit 1

	findImages "$imageQuery"; shift
	local images=("${resultsImages[@]}")

	if [ "${#images[@]}" -eq 0 ]; then
		echo "error while editing tags: no images found for input '$input'"
		exit 1
	fi

	echo "Tagging the following image(s):"
	for image in "${images[@]}"; do
		echo "${image#$wallpaperDir/images/}"
	done | column
	echo

	echo "With the following tag(s):"
	for tag in "${tags[@]}"; do
		echo "$tag"
	done | column
	echo

	read -rp "Continue tagging? [y/n]: "
	[ "$REPLY" == "${REPLY#[yY]}" ] && echo "Not tagging." && return 0

	IFS=$'\n'
	for image in "${images[@]}"; do
		for tag in "${tags[@]}"; do
			local imageBasename="$(basename "$image")"
			local tagged_image_path="$wallpaperDir/tags/$tag/$imageBasename"

			[ "$action" == "tagAdd" ] && if [ ! -f "$tagged_image_path" ]; then
				printf '%b\n' "tagging '$imageBasename' with tag: $tag"
				mkdir -p "$wallpaperDir/tags/$tag"
				ln -s "$image" "$tagged_image_path"
			else
				printf '%b\n' "skipping '$imageBasename': already has tag $tag"
			fi

			[ "$action" == "tagRemove" ] && if [ -f "$tagged_image_path" ]; then
				printf '%b\n' "removing tag $tag from image $imageBasename..."
				rm "$tagged_image_path"
			else
				printf '%b\n' "skipping: '$imageBasename' does not have tag $tag"
			fi
		done
	done
}

case "$action" in
	printColors) printColors ;;
	list) list "$1";;
	listImages) listImages "$1" ;;
	imageDelete) imageDelete "$1" ;;
	preview) preview "$1" ;;
	cycle) cycle "$1" ;;
	rice) rice "$1" ;;
	tagView) tagView "$1" ;;
	tagDelete) tagDelete "$1" ;;
	cleanTags) cleanTags ;;
	tagAdd|tagRemove) editImageTags "$action" "$1" "$2" ;;
	rename) imageRename "$1" "$2" ;;
esac
