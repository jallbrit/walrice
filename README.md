# walrice

```
                                        _     _        
                           __ __ ____ _| |_ _(_)__ ___ 
                           \ V  V / _` | | '_| / _/ -_)
                            \_/\_/\__,_|_|_| |_\__\___|
               ___                                              :.          
            .-9 9 `\                                          ....          
          =(:(::)=  ;                                        .~.            
            ||||     \                                     .:..           ..
            ||||      `-.                      ....:::=::....         ..: ..
           ,\|\|         `,                 ...=o==oo==o=~~:..    ... ..    
          /                \              ..~~:o==ooo==:~:=oo=  .:...       
         ;                  `'---.,      ..~=oooooooo=:.:oo=~:~~..          
         |                         `\    ..:oo=:oooooo==o:~~:=o:..          
         ;                     /     |     .:==oooooo===o==oo=:~            
         \                    |      /     . ..~~~::::::::~~~..             
          )           \  __,.--\    /       . ............... .             
       .-' \,..._\     \`   .-'  .-'          .. ............               
      `-=``      `:    |   /-/-/`                    ..                     
                   `.__/                 
```

## Introduction

`walrice` creates, manages, and deploys system themes, utilizing "tags" to categorize images. The name comes from `wal`, the backend of `walrice`, and "rice," which is a word the \*nix community uses to describe theming your system.

## How it Works

`walrice` is essentially a symlink manager with a bunch of extra capabilities. It works by first knowing where to find images and tags, which is `~/Pictures/wallpapers` by default. `walrice` then creates a folder for each tag you create. When you tag an image, you're actually creating a symlink inside that tag's folder linking to the original image. This way, all your images stay in one folder.

## Dependencies

* `fzf`
* `xsettingsd`
* `qt5ct`
* `catimg`

For Debian-based distributions, run:

```bash
sudo apt install fzf xsettingsd qt5ct catimg
```

## Installation

To download this project and install:

```bash
git clone https://gitlab.com/jallbrit/walrice
cd walrice
make install
```

## Usage

```
Usage: wr [OPTION]... [TAG|TAG/IMAGE|IMAGE]...
Create, manage, and deploy themes. By default, generate a theme based on a given
tag and deploy it by changing the wallpaper and terminal colors.

optional args:
  -h, --help            display this help
  -v, --verbose         make output more verbose
  -d, --directory DIR   set wallpaper directory, which must contain directory
                        named 'images'. Default is '~/Pictures/wallpapers'
  -g, --gtk             generate gtk and qt themes and live reload applications
      --skip            skip changing colors in terminals
  -w, --wall            just set wallpaper and exit
  -W, --wallmode MODE   set how wallpaper will fit screen (see below)
  -f, --geometry=WxH+x+y
                        set feh --geometry; see 'man feh' for more details
  -c, --colors          print current theme's colors
  -p, --preview         preview images inside terminal (requires catimg)
  -l, --list [TAG]      list all tags or images inside tag
  -L, --list-images     list all images
  -r, --recursive       recursively rice to all images inside a tag
  -s, --waittime TIME   time (in seconds) to wait between images during a
                        recursive rice
  -t, --tag IMAGE [TAG]...
                        add tag to image
  -T, --remove-tag IMAGE [TAG]...
                        remove tag from image
  -v, --view            view image's tags
  -D, --delete-tag      delete tag
  -I, --delete-image    delete image
  -m, --rename IMAGE NEW-NAME
                        rename image to new name, and fix tags to point to new image
  -C, --clean           clean up tag database, removing empty tags and dead links

wallpaper modes:
  center: center image on background, adds border if too small
  fill:   preserves aspect ratio, zoom until it fits all edges
  max:    preserves aspect ratio, zoom until it fits one edge
  scale:  fit image to exact edges, stretching it when necessary
  tile:   tile image if smaller than screen
```
