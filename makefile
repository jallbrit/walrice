.PHONY: install
install:
	chmod +x wr.sh
	cp wr.sh ~/.local/bin/wr

.PHONY: uninstall
uninstall:
	rm -f ~/.local/bin/wr
